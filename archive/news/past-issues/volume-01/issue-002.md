Author: Noel Torres

Date: 2014-12-09 03:54 -000

To: dng

Subject: [Dng] Devuan Weekly News II


Hi all, and welcome to Devuan Weekly News II. Another monday night, another
sleepless night to compile all the threads for your benefit.

More hands are needed on DWN if I want to sleep :D

Enjoy.

My editorial note for this week is that we have seen less harsh than on DWN I
but more polical standing messages (more comments from me at the end).

Maybe you remember from issue I [1] that there was a discussion about changing
Devuan's name. It has been stated now [2] that there will be no change at all.

[1] https://lists.dyne.org/digest/1417518895.14243_1.navi:2,S.html

[2] https://lists.dyne.org/lurker/message/20141202.093107.c1c17ea8.en.html

There has been also a discussion about the Reply-to header in messages to the
list [3] but it has also been stated (in the same [2]) that it will not be
changed.

[3] https://lists.dyne.org/lurker/message/20141201.215243.f4e93281.en.html

It has also been commented [4] if we should continue using freenode ans it
does not allow tor:

[4] https://lists.dyne.org/lurker/message/20141202.151336.71f443f3.en.html

There is an interesting ongoing discussion from [5] about how to name the
Devuan releases. There have been options based on italian food, Monsters, Inc.
names ([6]), star names ([7]), songs from Frank Zappa ([8]), names from other
films ([9]), and names related to Computing History and Unix creation at [10].

[5] https://lists.dyne.org/lurker/message/20141207.125705.b8eb0f0a.en.html

[6] https://lists.dyne.org/lurker/message/20141208.010348.a20f41c9.en.html

[7] https://lists.dyne.org/lurker/message/20141207.220616.3806dfe6.en.html

[8] https://lists.dyne.org/lurker/message/20141207.170242.179e2e8c.en.html

[9] https://lists.dyne.org/lurker/message/20141208.003959.43c6f4bf.en.html

[10] https://lists.dyne.org/lurker/message/20141208.115652.1866305d.en.html

It has also been discussed ([11]) what to do about GTK, since it is controlled
by the GNOME Foundation who pushes systemd on. Issue started with a discussion
([12]) about Gnome and GDM, and how GNOME Project behaves. Another statament
was needed at [13] abouth the relation between Devuan on one hand and systemd
and Gnome on the other.

[11] https://lists.dyne.org/lurker/message/20141206.135753.2d326577.en.html

[12] https://lists.dyne.org/lurker/message/20141205.125409.30d7a092.en.html

[13] https://lists.dyne.org/lurker/message/20141205.172126.2118aad0.en.html

Another interesting discussion started at [14] about Devuan having a
Constitution, but it soon derived to a discussion about if we should have non-
free and contrib sections ([15]) and the usage of the word "antisocial" ([16])
and the real need for non-free firmware ([17]), but it was stated that we will
follow current Debian usage at least until Devuan 1.0 is liberated ([18]).

[14] https://lists.dyne.org/lurker/message/20141205.213027.15680885.en.html

[15] https://lists.dyne.org/lurker/message/20141206.001640.55e0f9cd.en.html

[16] https://lists.dyne.org/lurker/message/20141206.062355.4d1fc2d7.en.html

[17] https://lists.dyne.org/lurker/message/20141206.205110.80b53316.en.html

[18] https://lists.dyne.org/lurker/message/20141206.100602.630cae96.en.html

There has been another trolling thread at [19] and I would not be mentioning
it here, but the thread raised the very important issue (best expressed by
Eric S. Raymond at [20]) of the real extent of freedom to speech. Please
remember not to feed the troll, but what is right needs to be acknowledged,
"For if we censor public speech in aid of our own political positions, we
forfeit the right to object when others censor our public speech in aid of
theirs. There is only one place that road can end, and it’s not anywhere we
want to be." (ESR).

[19] https://lists.dyne.org/lurker/message/20141203.042211.86faadb3.en.html

[20] http://esr.ibiblio.org/?p=1310

There was another request for a TODO list ([21]) and another request for
organization at [22], and a thread that could have been technical about the
importance of properly dealing with policikit ([23]), but which ended in
another useless discussion about systemd hate. And obviously, silence creates
noise ;) ([24]).

[21] https://lists.dyne.org/lurker/message/20141203.075622.6c702582.en.html

[22] https://lists.dyne.org/lurker/message/20141206.210817.503cdb9e.en.html

[23] https://lists.dyne.org/lurker/message/20141203.091131.f6bcb237.en.html

[24] https://lists.dyne.org/lurker/message/20141204.165240.0427e65f.en.html

There was also a discussion about moving discussion from a mailing list to a
Forum or something similar ([25])

[25] https://lists.dyne.org/lurker/message/20141204.231402.bfb047cf.en.html

Also a discussion about scripting languages and typing, at [26].

[26] https://lists.dyne.org/lurker/message/20141206.030135.8c980889.en.html

Last week a thread started about how to remove libsystemd0 at [27], with some
advice.

[27] https://lists.dyne.org/lurker/message/20141202.214840.48476b52.en.html

Some extra messages regarding Wikipedia's page about Jaromil (which may have
been in their own thread) were at [28].

[28] https://lists.dyne.org/lurker/message/20141208.172956.eed0680c.en.html

That's all. Too many stataments, the project seems to be dictatorial, at least
as it is going now. Maybe this is needed for the project to settle (and I
mostly agree with them all), but the lack of transparency metioned in DWN I is
now more apparent, if possible.

Thanks for reading so far

Noel

er Envite

self-appointed DWN sleepless redactor and editor 