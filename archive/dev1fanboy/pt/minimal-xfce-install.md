# Instalação minimalista do Xfce
Este documento descreve como executar uma instalação do xfce4 minimalista, incluindo alguns extras opcionais que frequentemente são desejados.

## Instalando o Xfce
Instale os pacotes base que permitirão que comece a utilizar seu novo ambiente gráfico.

`root@devuan:~# apt-get install xfce4-panel xfdesktop4 xfwm4 xfce4-settings xfce4-session`

Talvez queira os seguintes pacotes para um desktop mais completo.

`root@devuan:~# apt-get install tango-icon-theme xfwm4-themes xfce4-terminal xfce4-appfinder thunar xfce4-power-manager ristretto`

### Adicionando suporte para montagem automática
Instale os pacotes necessários do thunar (o gerenciador de arquivos do xfce) para habilitar montagem automática.

`root@devuan:~# apt-get install thunar-volman gvfs policykit-1`

Depois que iniciar seu ambiente gráfico, abra o gerenciador de configurações do Xfce e terá acesso às configurações dessa funcionalidade.

### Adicione um gerenciador de exibição
Se um gerenciador de exibição é necessário, recomendamos o slim, que é padrão no Devuan.

`root@devuan:~# apt-get install slim`

Agora execute update-alternatives para definir o x-session-manager para o xfce4-session.

`root@devuan:~# update-alternatives --config x-session-manager`

### Usando o xfce sem um gerenciador de exibição
Inicie a sessão como um usuário comum, e no console execute o script startxfce4.

`user@devuan:~$ startxfce4`

---
Traduzido por Douglas H. Silva

<sub>**Esta obra está licenciada com uma Licença Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR)]. Todas as marcas são propriedades de seus respectivos donos. Este trabalho é fornecido "COMO É" e vem com absolutamente NENHUMA garantia.**</sub>
