These instructions are for migrating from the Debian Jessie release only. When migrating to Beowulf the instructions are specific to the Debian release you're coming from and should be followed only as written.

# Migrate from Debian Jessie to Beowulf

It's necessary to [configure your network](network-configuration.md) before you begin, otherwise you will lose network access during the migration.

The best and easiest way to begin migration is to first install sysvinit-core.

`root@debian:~# apt-get install sysvinit-core`

Then reboot so that sysvinit is pid 1.

`root@debian:~# reboot`

Gnome users will have to remove the gnome tweak tool in order to avoid package blocks. This may have an effect on your desktop, but after migration we will install the Devuan Gnome packages if wanted.

`root@debian:~# apt-get purge gnome-tweak-tool`

Now let's change over to the Devuan Beowulf repositories.

`root@debian:~# editor /etc/apt/sources.list`

Make your sources.list look like the one provided. Comment out all other lines.

~~~
deb http://deb.devuan.org/merged beowulf main
deb http://deb.devuan.org/merged beowulf-updates main
deb http://deb.devuan.org/merged beowulf-security main
#deb http://deb.devuan.org/merged beowulf-backports main
~~~

Update the package lists from the Beowulf repository.

`root@debian:~# apt-get update`

Install the Devuan keyring so packages can be authenticated.

`root@debian:~# apt-get install devuan-keyring --allow-unauthenticated`

Update the package lists again so that all packages from here on in are authenticated.

`root@debian:~# apt-get update`

If you chose to use wicd during network configuration, you will need to perform a normal upgrade against the Beowulf repository first.

`root@debian~# apt-get upgrade`

Now you can perform the migration.

`root@debian:~# apt-get dist-upgrade`

If the upgrade stops at any time you should fix the broken packages then start the upgrade again.

`root@debian:~# apt-get -f install`  
`root@debian:~# apt-get dist-upgrade`

You can now remove systemd.

`root@debian:~# apt-get purge systemd`

Those wanting to use Gnome should be sure to install the task package now.

`root@devuan:~# apt-get install task-gnome-desktop`

Or you can use the default Devuan desktop XFCE.

`root@devuan~# apt-get install task-xfce-desktop`

The migration will likely leave behind some orphaned packages and unusable archives. For a more minimalistic system, you can remove these now.

`root@devuan:~# apt-get autoremove --purge`  
`root@devuan:~# apt-get autoclean`

---

<sub>**This work is released under the Creative Commons Attribution-ShareAlike 4.0 International [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)] license. All trademarks are the property of their respective owners. This work is provided "AS IS" and comes with absolutely NO warranty.**</sub>
