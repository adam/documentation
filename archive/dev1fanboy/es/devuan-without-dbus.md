# Devuan sin D-Bus
Este documento describe el procedimiento para remover D-Bus de Devuan incorporando el cambio a un gestor de ventanas
liviano, un nuevo navegador Web y una solución alternativa a los mecanismos de auto-montado dependientes de D-Bus.

## Seleccionar un gestor de ventanas
A diferencias de los entornos de escritorio, la mayoría de los gestores de ventanas no dependen de D-Bus, con lo cual
se debe instalar un gestor de ventanas.

* blackbox
* fluxbox
* fvwm
* fvwm-crystal
* openbox

### Instalar y configurar Fluxbox

Se utiliza Fluxbox ya que es simple e intuitivo.

`root@devuan:~# apt-get install fluxbox`

Hacer que Fluxbox sea el gestor de ventanas por defecto cuando usamos el script startx.

`user@devuan:~@ echo "exec fluxbox" >> .xinitrc`

Invocar el script para utilizar Fluxbox.

`user@devuan:~@ startx`

Una buena opción para la gestión de pantalla es WDM.

`root@devuan:~# apt-get install wdm`

## Seleccionar un navegador Web

Hay unos pocos navegadores que dependen de componentes de dbus, sin embargo algunos son mejores que otros. Es posible
elegir entre los siguientes.

* midori
* surf
* links2
* dillo
* lynx

Uno de los mejores navegadores que no requieren dbus es midori.

`root@devuan:~# apt-get install midori`

## Eliminar D-Bus de Devuan
Ahora es posible eliminar dbus de nuestra instalación de Devuan.

`root@devuan:~# apt-get purge dbus`

Además debemos eliminar cualquier paquete huérfano que haya quedado.

`root@devuan:~# apt-get autoremove --purge`

## Una alternativa simple al auto-montado
Sin D-Bus no tendremos la capacidad de auto-montaje de dispositivos en la mayoría de los gestores de archivos, ya que
tales mecanismos requieren D-Bus y otros más simples para montar dispositivos no siempre están implementados. Vamos a
configurar los puntos de montaje de manera manual, de forma que sea posible montar volúmenes en los gestores de
archivos con unos pocos clics.

### Puntos de montaje manuales
Crear un directorio para definir un nuevo punto de montaje.

`root@devuan:~# mkdir /media/usb0`

Crear una copia de seguridad del archivo fstab antes de proceder.

`root@devuan:~# cp /etc/fstab /etc/fstab.backup`

A continuación editar el archivo fstab.

`root@devuan:~# editor /etc/fstab`

Se necesita agregar un punto de montaje para un dispositivo USB al final del archivo. Asegurarse de agregar la opción
`user` para que los usuarios no privilegiados (no root) puedan montar el dispositivo.

~~~
/dev/sdb1       /media/usb0     auto    user,noauto     0 0
~~~

Los nombres de nodo para los dispositivos USB pueden variar de acuerdo a cada configuración. Es posible saber qué
nombres serán utilizados conectando el dispositivo y utilizando la herramienta `lsblk`.

Conectar un dispositivo USB para verificar la configuración.

`user@devuan:~$ mount -v /media/usb0`

`user@devuan:~$ umount -v /media/usb0`

### Seleccionar un gestor de archivos
Para contar con un gestor de archivos que pueda montar y desmontar dispositivos de acuerdo a la configuración del
archivo fstab es posible instalar Xfe.

`root@devuan:~# apt-get install xfe`

Un gestor de archivos minimalista y basado en ncurses es Midnight Commander.

`root@devuan:~# apt-get install mc`

---

<sub>Traducido por Emiliano Marini.</sub>

<sub>**Este trabajo es liberado bajo la licencia Atribución-CompartirIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.es)]. Todas las marcas registradas son propiedad de sus respectivos dueños. Este trabajo se provee "COMO TAL" y NO posee garantía en absoluto.**</sub>

