# Λογισμικό ελεύθερο του D-Bus στη Devuan
Κατάλογος λογισμικού που περιλαμβάνεται στη Devuan, εντελώς ελεύθερου της επιβάρυνσης του D-Bus.

## Διαχειριστές παραθύρων
Προτάσεις από τη κοινότητα της Devuan

* fluxbox
* blackbox
* openbox
* fvwm 
* fvwm-crystal
* icewm
* metacity
* evilwm
* windowmaker
* mutter
* sawfish
* icewm
* dwm
* i3
* wmaker

## Διαχειριστές αρχείων
* xfe
* pcmanfm
* rox-filer
* mc
* gentoo
* fdclone
* s3dfm
* tkdesk
* ytree
* vifm

## Διαχειριστές οθόνης
* nodm
* ldm
* xdm
* wdm

## Φυλλομετρητές Διαδυκτίου
* chimera2
* dillo
* edbrowse
* lynx
* elinks
* links
* links2
* elinks
* w3m
* surf
* netsurf
* uzbl

## Συσκευές αναπαραγωγής μουσικής
* alsaplayer
* deadbeef
* cmus
* cdcd
* lxmusic
* randomplay

## Συσκευές αναπαραγωγής πολυμέσων
* totem
* xine-ui
* mplayer2
* mpv
* melt

## Προγράμματα IRC
* weechat
* irssi
* ekg2-ui-gtk
* scrollz
* loqui

---

<sub>**Αυτό το έργο διατίθεται υπό την άδεια χρήσης Creative Commons Αναφορά Δημιουργού - Παρόμοια Διανομή 4.0 Διεθνές [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.el)]. Όλα τα εμπορικά σήματα αποτελούν ιδιοκτησία των αντίστοιχων ιδιοκτητών τους. Αυτό το έργο παρέχεται "ΩΣ ΕΧΕΙ" και δεν προσφέρει ΚΑΜΙΑ απολύτως εγγύηση.**</sub>
