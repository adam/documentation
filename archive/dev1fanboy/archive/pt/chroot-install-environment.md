Esta documentação com instruções para chroot é experimental, e necessita de melhorias.

# Criando um ambiente de instalação chroot

O propósito deste documento é mostrar como criar uma instalação do Devuan que funciona separadamente do seu sistema atual. Isso pode ser útil para virtualização, ou para instalar certos pacotes sem afetar o sistema em execução. Em outras palavras, é útil para quaisquer propósitos de teste.

Para ilustrar melhor, vamos nos referir ao lançamento Ceres (instável) quando instalarmos o ambiente chroot. Para o caso em que você queira um lançamento diferente, substitua 'ceres' pelo lançamento escolhido.

## Crie a instalação chroot

Instale o pacote debootstrap que precisaremos para a instalação.

`root@devuan:~# apt-get install debootstrap`

Você precisará de um local para o sistema chroot, então deve criar uma pasta em algum lugar para isso.

`root@devuan:~# mkdir /mnt/ceres`

Agora complete o processo de instalação chroot usando debootstrap.

`root@devuan:~# debootstrap ceres http://pkgmaster.devuan.org/merged /mnt/ceres`

## Fazendo chroot na instalação

Quando usar o ambiente chroot você às vezes precisará ter certeza de que `dev`, `proc` e `sys` estão montados nele.

`root@devuan:~# mount -o bind /dev /mnt/ceres/dev`

`root@devuan:~# mount -o bind /sys /mnt/ceres/sys`

`root@devuan:~# mount -t proc proc /mnt/ceres/proc`

Faça chroot no ambiente para começar a usá-lo.

`root@devuan:~# chroot /mnt/ceres /bin/bash`

## Saindo do ambiente chroot

Quando terminar, você deve sair apropriadamente do ambiente chroot.

`root@chroot:~# exit`

`root@devuan:~# umount /mnt/ceres/dev`

`root@devuan:~# umount /mnt/ceres/sys`

`root@devuan:~# umount /mnt/ceres/proc`

---
Traduzido por Douglas H. Silva

<sub>**Esta obra está licenciada com uma Licença Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR)]. Todas as marcas são propriedades de seus respectivos donos. Este trabalho é fornecido "como é" e vem com absolutamente NENHUMA garantia.**</sub>
