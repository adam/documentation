# Instalação minimalista do Devuan Jessie
Este documento descreve como executar uma instalação minimalista do Devuan Jessie a partir de uma mídia inicializável como, por exemplo, um CD/DVD-ROM ou um pendrive.

## Pré requisitos
Este método de instalação é destinado a usuários avançados que buscam otimizar seus sistemas. Você deve portanto estar familiarizado com a instalação do Devuan a partir do instalador avançado e com outras tarefas de administração geral, assim como estar comfortável em usar o terminal.

## Arquiteturas suportadas
* amd64
* i386

## Introdução
Utilizaremos o instalador como um meio de facilmente configurar o sistema; por outro lado, vamos evitar a seleção padrão de software. Para isso instalaremos apenas o mais essencial dos pacotes permitidos pelo apt, depois faremos um chroot na instalação. O instalador será usado para configurar o básico, que será transferido para a nossa instalação no final desse processo.

## Primeiros passos
Assim que tiver inicializado a partir da mídia, inicie o instalador selecionando a opção de instalador avançado (expert install).

Avance pelas etapas do instalador até que o disco tenha sido particionado e formatado, então pare de usar o instalador, já que faremos todo o resto manualmente.

Completaremos o primeiro estágio da instalação com o debootstrap, depois fazer chroot no ambiente de instalação e continuar de lá. Para aqueles não familiarizados com o debootstrap, é a ferramenta responsável por instalar sistemas baseados no Debian.

## Faça debootstrap no alvo
Primeiro entre em um terminal. Pressione Alt + F2 agora seguido de ENTER para ativar o console.

O instalador já nos ajudou a configurar e montar as partições, então o disco está pronto para receber o sistema. Confirme isso observando a estrutura de diretório de /target.

`~ # ls /target`

Antes de prosseguir, certifique-se de que tem as mais recentes chaves gpg necessárias para o debootstrap.

`~ # gpg --recv-keys 95861109`

Agora iremos executar o debootstrap no /target completando o primeiro estágio da instalação. É importante incluir a opção --variant=minbase já que ela nos dará uma seleção minimalista de pacotes. Incluiremos o editor nano aqui por conveniência, mas você pode substituir por outro de sua preferência.

`~ # debootstrap --variant=minbase --include=nano jessie /target http://pkgmaster.devuan.org/merged`

## Fazendo chroot no destino
Antes de prosseguir com o processo, precisamos executar um 'chroot' no ambiente de instalação para que possamos continuar configurando e instalando pacotes.

Precisamos antes tornar proc, dev e sys disponíveis ao ambiente chroot.

`~ # mount -t proc proc /target/proc`

`~ # mount -o bind /dev /target/dev`

`~ # mount -o bind /sys /target/sys`

Agora execute o chroot.

`~ # chroot /target /bin/bash`

## Removendo pacotes desnecessários
Agora você pode obter um sistema mais minimalista eliminando pacotes que não são necessários e que não podem ser removidos durante o processo de bootstrap.

Dependendo da sua configuração você pode não se importar com internacionalização no debconf. Se for o caso, isso pode ser removido.

`root@devuan:/# dpkg --purge debconf-i18n`

Como você só precisa de um pacote gcc-base, é seguro remover gcc-4.8-base em favor de gcc-4.9-base.

`root@devuan:/# dpkg --purge gcc-4.8-base`

## Configurando APT para excluir pacotes recomendados
Como deve saber, pacotes recomendados pesam bastante no sistema, adicionando recursos que raramente serão úteis. Outra vantagem de instalar por debootstrap é que você pode garantir que pacotes recomendados não pesem em seu sistema. Lembre-se de que a maioria dos navegadores (e o wget) requerem o pacote ca-certificates para verificar conexões SSL. Portanto você precisará instalar isso posteriormente para quaisquer sistemas que necessitarem. Não esqueça!

Use um editor de texto para fazer as mudanças necessárias

`root@devuan:/# editor /etc/apt/apt.conf.d/01lean`

Adicione as seguintes linhas.

~~~
APT::Install-Recommends "0";
APT::AutoRemove::RecommendsImportant "false";
~~~

## Atualizando o sistema
Antes de continuar você deve verificar se seu sistema está atualizado com as últimas correções de segurança.

Primeiro adicione os repositórios.

`root@devuan:/# editor /etc/apt/sources.list`

Confira se o seu sources.list possui essas linhas.

~~~
deb http://pkgmaster.devuan.org/merged jessie main
deb http://pkgmaster.devuan.org/merged jessie-updates main
deb http://pkgmaster.devuan.org/merged jessie-security main
deb http://pkgmaster.devuan.org/merged jessie-backports main
~~~

Atualize a partir dos repositórios para habilitá-los.

`root@devuan:/# apt-get update`

Agora você pode aplicar as atualizações disponíveis para os pacotes base que instalamos.

`root@devuan:/# apt-get dist-upgrade`

## Escolhendo uma interface debconf
Existem mais do que uma interface do debconf e você pode escolher entre elas com base em seu estilo, já que nenhuma está instalada no momento. Essa é uma boa oportunidade para customizar seu sistema.

### Whiptail
Whiptail é uma interface fácil de usar, semelhante ao dialog e é a interface debconf padrão no Devuan.

### Dialog
Dialog é semelhante ao whiptail e será familiar a muitos usuários que já executaram instalações não gráficas antes.

### Readline
A interface readline não é exatamente uma interface já que se comporta mais como uma lista de opções numéricas apresentadas para sua escolha. Essa interface utiliza o módulo Perl, então isso pode ser uma boa escolha para quem já possui outras utilidades com o Perl.

### Editor
Esse método de configurar pacotes não é uma interface, pois funciona executando um editor na primeira linha relevante do arquivo de configuração para o pacote instalado. Isso pode ser um ótimo meio de aprender mais sobre seu sistema, ou obter um controle mais minucioso se for necessário.

## Instale e configure a interface debconf
Uma boa escolha minimalista é a interface padrão whiptail.

`root@devuan:/# apt-get install whiptail`

A interface readline é uma boa escolha para quem prefere fazer as coisas pelo terminal.

`root@devuan:/# apt-get install libterm-readline-perl-perl`

Ou utilize o dialog em vez do whiptail, se preferir.

`root@devuan:/# apt-get install dialog`

Agora reconfigure o debconf para informá-lo qual interface você estará usando. 

`root@devuan:/# dpkg-reconfigure debconf`

## Adicionando os componentes de rede
Instale o mínimo de pacotes necessários para habilitar acesso à rede.

`root@devuan:/# apt-get install netbase net-tools ifupdown`

Alguns pacotes opcionais podem ser necessários, especialmente um cliente DHCP para configuração automática de rede.

`root@devuan:/# apt-get install isc-dhcp-client inetutils-ping`

## Instalando o kernel Linux
Para inicializar do seu novo sistema GNU/Linux é preciso instalar o kernel Linux.

```root@devuan:/# apt-get install linux-image-`dpkg --print-architecture` ```

## Instalando o gerenciador de inicialização
Utilizaremos o GRUB2 já que este ocupa menos espaço após instalado e possui mais suporte do que o lilo.

Se você estiver usando volumes lógicos, precisa instalar o pacote lvm2 antes.

`root@devuan:/# apt-get install lvm2`

Se você possui outro sistema operacional instalado nesta máquina, instale o os-prober para detectá-lo e adicioná-lo à lista de inicialização.

`root@devuan:/# apt-get install os-prober`

Agora instalaremos o gerenciador de inicialização.

`root@devuan:/# apt-get install grub2`

Geralmente você irá querer instalá-lo no setor MBR do primeiro disco, que possivelmente será identificado por /dev/sda.

## Extras opcionais
Alguns pacotes opcionais para a instalação base.

`root@devuan:/# apt-get install psmisc pciutils rsyslog less`

Isso é uma sugestão de pacotes que talvez deseje e que poderão ajudar no pós instalação.

`root@devuan:/# apt-get install man manpages lynx irssi`

Se acesso remoto ao shell for necessário, não se esqueça de instalar o servidor shell agora.

`root@devuan:/# apt-get install openssh-server openssh-client openssh-blacklist`

## Saindo do ambiente chroot (de modo limpo)
Primeiro saia do chroot.

`root@devuan:/# exit`

Desmonte proc, dev e sys.

`~ # umount /target/proc`

`~ # umount /target/dev`

`~ # umount /target/sys`

## Finalizando a instalação
É hora de dizer ao instalador para finalizar agora. Todos os arquivos de configuração restantes serão criados no sistema de destino, então esta etapa não deve ser omitida.

Retorne ao instalador pressionando Alt + F1 simultaneamente e pule para a etapa que finaliza a instalação.

Você será questionado sobre instalar o sistema base, mas deve recusar selecionando a opção voltar (back), que irá pular esta etapa. Isso deve ser feito na etapa do gerenciador de inicialização também, já que você o instalou manualmente.

---
Traduzido por Douglas H. Silva

<sub>**Esta obra está licenciada com uma Licença Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR)]. Todas as marcas são propriedades de seus respectivos donos. Este trabalho é fornecido "COMO É" e vem com absolutamente NENHUMA garantia.**</sub>
