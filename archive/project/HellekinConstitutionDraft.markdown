# Devuan Constitution

## Roots

(This section reminds points from other pages such as [ProjectDescription](ProjectDescription), the [home](home) for clarity, and will be removed)

+ Having a Debian Jessie that does not impose `systemd` for PID 1 to preserve diversity of init systems
+ To foster software freedom and user control
+ To stick with GNU, KISS, and UNIX philosophy
+ To be the continuity of and remain 100% fully compatible with Debian
+ To provide lighter governance
+ To serve users and sysadmins
+ To uphold the GNU system

Version 1 of the devuan constitution will be discussed with all
contributors, but final decision on adopting it will be done by the
VUA group that originally started the devuan fork.

## Version 0 (Proposed Draft)

1. [Preamble](#1-preamble)
2. [Project Goals](#2-project-goals)
3. [Software Freedom](#3-software-freedom)
4. [Social Contract](#4-social-contract)
5. [Governance](#5-governance)

## 1. Preamble

Software opens a new technological era in the evolution of Mankind.
Not all software is born equal.  As extensions of the human mind and
capacity to act and shape reality, software is a fundamental element
of the tool set of humans to bring forth unprecedented changes in our
lives.  Therefore we must apply thought and attention to software
development and we share responsibility, as users and developers of
software systems, to foster values of cooperation in the spirit of
science, human cultures, and the diversity of life.

The Devuan Project is a solidarity network of individuals who have made
a common cause to create a free operating system.

This document describes the [Project's primary
goals](#2-project-goals), its dedication to [software
freedom](#3-software-freedom), its promises to Devuan users and
contributors in the form of a [social contract](#4-social-contract),
and the [governance principles](#5-governance) to ensure durability and
formal decision-making in the Project.

## 2. Project goals

### 2.1. Build a Universal GNU Operating System

The Devuan Project was born as a fork of Debian, and considers itself
the legacy of the Debian Project.

Devuan is a free operating system (OS) for any computer system, from
home desktop to servers, from embedded devices to any sort of big
complex systems.  Our main goal is to provide an universal OS, easily
adaptable to any sort of use case and environment, following the UNIX
principles as guidelines, and fostering software freedom.

### 2.2 Embrace Diversity

An universal OS must protect freedom of choice.  Devuan provides not
only support for a large number of platform architectures, but also a
diversity of software solutions for achieving matching component
functionality.  The diversity of use cases, from home users to
developers and sysadmins, from common usage to minority usage must be
supported.  A minority of users cannot work in the way of a majority
of users, and vice-versa, a minority of users usually has good reasons
to do things in a different way.  Devuan fosters a diversity of
approaches to avoid technical lock-ins to specific implementations.

### 2.3 Uphold Harmonious Cooperation

The Devuan Project embraces an inclusive and peaceful space for
contributors and users.  It works to improve the relationship with
both upstream developers and downstream individuals and organizations.

Efforts are made to facilitate integration of software packages into
the distribution, maintain fluid contact with the upstream developers,
and collaborate on the long term to improve the experience of the
meta-system formed by the Devuan OS, the upstream software developers,
and the downstream derivative OS distributors, institutional and
individual users.

### 2.4 Maintain a Solid Base OS Framework

The Devuan Project seeks to provide a rock solid, stable, and
innovative base OS framework for derivative works (specialized
distributions, pre-configured subsets, etc.), professionals, and
enthusiasts.  Devuan upholds both technical excellence and societal
collaboration.

Documentation and interaction must facilitate understanding and
participation in the elaboration and distribution of free software.
The Devuan community must be attentive and listen to users, and
flexible to integrate their feedback.  The Devuan community encourages
and coordinates the development of participatory tools such as wikis
and customization tools such as the Devuan Blends to pursue these
goals.

## 3. Software Freedom

Devuan is a free <abbr>OS</abbr>, that means it's a distribution of free software.
Free software respects the user's freedom and community.  Software
freedom is defined as user control over their computing, and requires
the four freedoms to be well understood and respected.

- __Freedom 0__ is the freedom to run the program for any purpose.
- __Freedom 1__ is the freedom to study the source code of the program and
  change it.

These two freedoms allow any individual user to run, study, and change
a program to adapt it to their own desire.  But if you're not a
programmer, you cannot exert such individual control over your
computing.  Therefore an additional two freedoms are necessary to
achieve collective control over computing:

- __Freedom 2__ is the freedom to redistribute exact copies of the
  program.
- __Freedom 3__ is the freedom to redistribute modified copies of the
  program.

### 3.1. Source Code

Typically, a program comes in two forms: the _source code_ written by
the developers, and available in plain text, readable form if you know
the programming language; an _executable_, which is what users run,
and that is compiled from the source code, usually in binary form of
ones and zeros, readable only by machines.

Availability of the source code is a requirement for exerting freedom
1, and therefore studying and understanding what the program does.
The only alternative is to reverse-engineer the program to understand
how it works from exploring its binary form.  That is a difficult
process that may or may not give results.

### 3.2. Copyleft and Free Licenses

Like software, software licenses are not made equal, and range from
satanic (putting you under the control of the developer) to lax
(allowing patent treachery) to Copyleft.  The Devuan project
recommends Copyleft licenses and allows lax licenses.  See [next
section](#3-3-binary-blobs-and-limits-to-freedom) regarding non-free
licenses. [Copyleft
licenses](https://gnu.org/licenses/recommended-copylefts.html) protect
the users against legal and commercial attacks on their control over
their own computing, such as patent trolling, _tivoization_, etc.

Quoting from the [Quick Guide to the
GPLv3](https://gnu.org/licenses/quick-guide-gplv3.html):

> Developers who write software can release it under the terms of the
  GNU GPL.  When they do, it will be free software and stay free
  software, no matter who changes or distributes the program.  We call
  this copyleft: the software is copyrighted, but instead of using
  those rights to restrict users like proprietary software does, we
  use them to ensure that every user has freedom.

The Devuan project recommends GPLv3-compatible licenses in priority,
including AGPLv3 for code distributed over the network, or the
permissive Apache2 and Xfree86 1.1 (also known as "X11" or "MIT") licenses.

Other free software licenses exist that are more lax and do not offer
the same protections, for example the Artistic 2.0 license used by the
Perl project, or the Modified BSD license used by many BSD
derivatives.

The interested reader can refer to the [GNU Project's licenses
section](https://gnu.org/licenses) to learn more about free software
licensing and [licensing
recommendations](https://gnu.org/licenses/license-recommendations.html).

### 3.3. Binary Blobs and Limits to Freedom

The [GNU Free System Distribution
Guidelines](https://gnu.org/distros/free-system-distribution-guidelines)
requires a distribution to not refer to non-free software,
documentation on how to make such software work with the <abbr>OS</abbr>, nor to
provide assistance to enable compatible use with non-free software.

This is usually but wrongly seen as a "restriction" on the part of
free software advocates.  Although this has certainly strong practical
implications, we prefer to consider it as a goal towards which the
Devuan Project needs to tend.  The current state of commercial
technology often does not leave much choice to the users with the
constraints that the hardware they use impose on them.

The Devuan Project recognizes there is a practical contradiction
between the goals of software freedom and the goal of providing a
universal operating system.  When the choice is left between using a
non-free software component, and no computing at all, the Devuan
Project will prefer some computing, in the hope the non-free part will
become less important over time and will eventually disappear
completely.

The Devuan Project will steer towards free-software-compatible
hardware and provide support for such systems and platforms, but will
also provide transitory support for systems and platforms that require
binary blobs and non-free components to work.  As such, the Devuan
Project is not a free software distribution as defined by the GNU
FSDG, but is aiming to become one, and does not recommend the use of
non-free software where it is not absolutely necessary.

### 3.4. Licensing Recommendations

The Devuan Project follows the recommendations of the Free Software
Foundation regarding licensing.  A complete review of free software
licenses can be found on the GNU website, and the full text of each
license in the Free Software Directory.  Please refer to [section
3.2](#3-2-copyleft-and-free-licenses).

### 3.5. Autonomy and Interdependence

In addition to respecting the four freedoms, software freedom implies
non-discrimination of users: core software in Devuan should be "as
simple as possible, but not simpler" and hopefully follow the "do one
thing and do it well" UNIX principle where feasible.  This is to
prevent large interlocking cascades of dependencies on single
components that add superfluous complexity to the system.

Core software included in the main distribution should therefore:

- avoid requiring other independent parts of the system to function,
- provide best portability across architecture platforms,
- follow POSIX compliance, and
- adhere to standards that do not conflict with the project's goals.

## 4. Social Contract

Therefore the Devuan OS embraces plain text files as the universal
interface, especially for logs and system executables.

The Devuan Project will work with the Free Software Foundation and
other organizations, including the Debian Project, towards an entirely
free software and free hardware general purpose computing environment.

# 4.1. We Shall Uphold Software Freedom

The Devuan Project builds a free operating system that respects user's
freedom.  This is the _raison d'être_ of the project and won't change.
Our system will never require to run a non-free component.  But we
won't exclude users on the basis they need to run non-free software
either, although we won't recommend such use.

# 4.2. We Shall Contribute to the Free Software Movement

Devuan is meant as a base <abbr>OS</abbr> for others to build upon.
Cooperation is built-in.  We shall remain true to software freedom and
foster user freedom and control over one's own computer, component reuse
and portability over technical cleverness and excellence that may
compromise such freedom.  When we write a software component for
Devuan, we shall license it according to [section
3](#3-software-freedom) of this document.

# 4.3. We Shall Not Hide Problems

We believe in conversation, collaboration, and full disclosure.  We
recognize human fallibility and accept our mistakes responsibly.  We
shall maintain public mailing lists and bug trackers to communicate
bugs to all users and upstream authors of software included in our
system, as well as provide secure channels and protocols to report
security issues responsibly.  We shall listen to arguments and respond
to inquiries promptly in the hope the community will help solve any
problem that may appear, rather than hiding anything susceptible to
build up and snowball into mayhem.  That includes personal and
organizational conflicts of interest.

# 4.4. We Shall Serve Our Users

The Devuan Project is in the service of our users, from all origins
and all paths of life.  We won't discriminate anyone for any
reason.  Individuals may hold different opinions and uphold antagonist
perspectives, but the only discriminating line we chose to draw within
the Project is to support software freedom: we put users first and
foremost.

# 4.5. We Shall Not Exclude Anyone

Discrimination over ethical, political, religious, national, or other
arguments do not belong to software freedom.  The subject of software
freedom is the individual user and their community in their capacity
to control their own computing needs.  Drawing a line as to who can
use or not the software is to open Pandora's box: we won't do that.
We expect that Devuan users also won't discriminate others.  

We acknowledge that some of our users require the use of works that do
not conform to the ideals of free software.  Devuan maintains
_contrib_ and _non-free_ areas in our archive for these works.  The
packages in these areas are not part of the Devuan system, although
they have been configured for use with Devuan.  We encourage CD
manufacturers to read the licenses of the packages in these areas and
determine if they can distribute the packages on their CDs.  Devuan
developers willing to include their package in the _main_ area must not
include any dependency on packages from _contrib_ and _non-free_.

Non-free works are not part of Devuan, and therefore are not burdened
onto the free software community.  A special fund for transitory
support of non-free works is setup to:

- enable users who need such dependencies now to obtain them
  seamlessly using a normal Devuan installation.
- maintain the Devuan-compatible infrastructure to host non-free
  software, characterize its injustice, and promote free software
  alternatives.
- enable users to realize their need and pool their effort in order to
  collaborate and provide for free alternatives.  
- raise awareness in the industry for the need to support software
  freedom natively, from firmware to the boot sequence to
  applications.

This "[Escape Fund](EscapeFund)" shall be taken in charge by the
Devuan Project as a distinct project and progressively granted autonomy so
that the amount of non-free software necessary to run systems
diminishes over time.  Development and support infrastructure of the
Devuan project shall be made to work seamlessly with this
infrastructure so that it does not become a burden for users either.

## 5. Governance

One of the stated goals of the Devuan Project is to provide a lighter
decision-making system than the Debian Project.  The principles set
forth in this Constitution should be kept in mind when making
decisions, and notably, one should ask: "how does this benefits users'
freedom? How does this impedes users' freedom?", and a balance should
be preferred over structure and bureaucracy.

### 5.1. Collective Decisions

Decisions are usually taken by action and affinity.  Nobody can
possibly follow each and every decision.  Therefore it is important
that changes are thoroughly discussed, pondered, and developed
publicly using the mailing lists.  Decisions affecting individual
packages should be made among developers and their users.  Decisions
affecting the whole distribution should be made after an acceptable
time has passed and people have been given a chance to review and
understand the rationales and consequences of changes.

### 5.2. Individual Roles

TBD.

- no personality cult
- delegates
- coordination roles
- collegial roles

### 5.3. The VUA Group

TBD

- veto

### 5.4. Leadership

TBD

- decentralized
- long term
- coordination
- orientation

### 5.5. Conflict Resolution

TBD

- all users first
- freedom second
- some users third

- keep it civil
- when heating, keep it private and intimate
