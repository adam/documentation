# Migrando para o Devuan Jessie com minimalismo em mente
Este guia irá instruí-lo pelo que será necessário para migrar para o Devuan, configurar o apt em favor de uma aproximação minimalista, remover D-Bus do sistema e gerenciar a conexão à rede.

## Migrando para o Devuan
Para iniciar a migração, comece editando o arquivo sources.list para que possamos atualizar os índices de pacotes com o apt-get.

`root@debian:~# editor /etc/apt/sources.list`

Desabilite quaisquer linhas obsoletas do arquivo e adicione as seguintes.

~~~
deb http://pkgmaster.devuan.org/merged jessie main
deb http://pkgmaster.devuan.org/merged jessie-updates main
deb http://pkgmaster.devuan.org/merged jessie-security main
deb http://pkgmaster.devuan.org/merged jessie-backports main
~~~

Atualize os índices de pacotes para que possamos buscar atualizações dos repositórios do Devuan.

`root@debian:~# apt-get update`

Antes de instalar qualquer novo pacote, precisamos do chaveiro do Devuan, que irá garantir autenticação dos pacotes

`root@debian:~# apt-get install devuan-keyring --allow-unauthenticated`

Agora que o chaveiro está instalado, você deve atualizar os índices novamente.

`root@debian:~# apt-get update`

Finalize o processo de atualização.

`root@debian:~# apt-get dist-upgrade`

## Configurando para minimalismo
Graças a uma dica do [TheFlash] você pode remover excessos do seu sistema de um jeito bem elegante. Configuraremos o apt para tratar pacotes recomendados como não importantes.

`root@devuan:~# editor /etc/apt/apt.conf.d/01lean`

Adicione as seguintes linhas:

~~~
APT::Install-Recommends "0";
APT::AutoRemove::RecommendsImportant "false";
~~~

Embora a maioria dos pacotes não sejam de natureza importante, existem alguns pacotes que você deve proteger contra remoção.

Para a segurança do seu navegador e outras aplicações, devemos ter certeza de que certificados SSL estarão sempre disponíveis. Instale manualmente o pacote ca-certificates. Apenas pule esta etapa se souber o que está fazendo.

`root@devuan:~# apt-get install ca-certificates`

O pacote de certificados SSL será agora marcado como um pacote manualmente instalado, em vez de uma dependência ou pacote recomendado. Se a próxima etapa mostra pacotes que você deseja manter, poderá fazer o mesmo a eles antes de confirmar qualquer remoção.

`root@devuan:~# apt-get autoremove --purge`

## Devuan sem D-Bus
Remover o dbus é mais complicado e traz efeitos colaterais.

### Montando volumes como usuário
Uma alternativa à montagem automática do D-Bus é configurar os pontos de montagem você mesmo, e instalar um gerenciador de arquivos que pode montar volumes sem o D-Bus.

Já que editaremos o fstab, faça um backup desse arquivo antes de prosseguir.

`root@devuan:~# cp /etc/fstab /etc/fstab.backup`

Agora pode editar o seu fstab.

`root@devuan:~# editor /etc/fstab`

Insira o seguinte ao fstab, adaptando as informações para corresponder ao seu caso. Não esqueça da opção `user` para permitir que usuários não root possam montar os discos.

~~~
/dev/sdb1        /media/usb0    auto    user,noauto    0 0
~~~

Você pode plugar um pendrive, por exemplo, e usar o utilitário `lsblk` para determinar o identificador correto do dispositivo.

Crie o ponto de montagem onde dispositivos serão montados.

`root@devuan:~# mkdir /media/usb0`

Plugue um pendrive e teste suas modificações como usuário comum.

`user@devuan:~$ mount -v /media/usb0`

`user@devuan:~$ umount -v /media/usb0`

### Instalando software independente do D-Bus
A maioria dos ambientes de desktop requerem dbus, então um gerenciador de janelas deve ser escolhido. Usaremos o fluxbox por ser intuitivo, leve e flexível.

`root@devuan:~# apt-get install fluxbox menu fbpager feh`

Use o update-alternatives para definir startfluxbox como seu gerenciador de janelas padrão.

`root@devuan:~# update-alternatives --config x-window-manager`

Se você deseja incluir um gerenciador de janelas, pode usar o WDM.

`root@devuan:~# apt-get install wdm`

Caso contrário, poderá iniciar o script startx, que deve ser invocado após logar em sua conta de usuário pelo console.

`user@devuan:~$ startx`

Para um gerenciador de arquivos que possa montar dispositivos removíveis sem um montador automático, poderá usar o xfe.

`root@devuan:~# apt-get install xfe`

Uma boa escolha de navegador é o firefox-esr, por não depender diretamente do dbus.

`root@devuan:~# apt-get install firefox-esr`

### Configure a rede
Em vez de usar um gerenciador de redes dependente do dbus, configuraremos a rede para uso com múltiplas interfaces manualmente.

`root@devuan:~# editor /etc/network/interfaces`

Aqui está uma configuração para múltiplas redes sem fio na mesma interface. Adicionando uma estrofe para uma rede que você só usa de vez em quando, você pode realizar uma troca rápida entre redes quando for desejado. Para mais informações, veja a [referência do debian](https://www.debian.org/doc/manuals/debian-reference/ch05.pt.html#_the_manually_switchable_network_configuration) sobre configuração comutável de redes.

~~~
allow-hotplug wlan0
iface wlan0 inet dhcp
        wpa-ssid myssid
        wpa-psk mypassphrase

iface work inet dhcp
        wpa-ssid myssid
        wpa-psk mypassphrase
~~~

Então, por exemplo, você pode mudar para a rede 'work' usando `ifdown wlan0` e `ifup wlan0=work` como root.

Conexões cabeadas são muito mais simples de configurar.

~~~
# Configuração automática de rede, iniciada apenas quando um link é detectado.
allow-hotplug eth0
iface eth0 inet dhcp

# Configuração estática de rede, sempre ativada durante a inicialização.
auto eth1
iface eth1 inet static
        address 192.168.1.5
        netmask 255.255.255.0
        gateway 192.168.1.1
~~~

Para mais informações sobre interfaces, veja `man 5 interfaces`.

## Finalizando
Uma reinicialização é necessária para remover o systemd, pois ainda está rodando como pid1.

`root@devuan~# reboot`

Agora vocẽ pode remover o systemd e o dbus com segurança.

`root@devuan:~# apt-get purge systemd systemd-shim libsystemd0 dbus`

O desktop GNOME é atualmente impossível de ser usado sem o systemd. Podemos utilizar uma expressão regular para capturar os pacotes GNOME e removê-los de uma só vez.

`root@devuan~# apt-get purge .*gnome.*`

Pode ser necessário proteger o pacote xorg contra remoção.

`root@devuan~# apt-get install xorg`

Você pode querer limpar seu sistema de pacotes recomendados e órfãos novamente.

`root@devuan:~# apt-get autoremove --purge`

Essa é uma boa hora de remover velhos pacotes arquivados pelo APT da sua instalação anterior.

`root@devuan:~# apt-get autoclean`

---
Traduzido por Douglas H. Silva

<sub>**Esta obra está licenciada com uma Licença Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR)]. Todas as marcas são propriedades de seus respectivos donos. Este trabalho é fornecido "COMO É" e vem com absolutamente NENHUMA garantia.**</sub>
