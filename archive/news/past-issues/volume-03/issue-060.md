# Devuan News Issue LX

__Volume 03, Week 4, Devuan Week 60__

Released 12016/01/31 [HE](why-he)

https://git.devuan.org/devuan-editors/devuan-news/wikis/past-issues/volume-03/issue-060

## Editorial

In this issue, we can feel the _beta release approaching_.  Desktop
branding is approaching completion, the repositories are reaching
their stable configuration, Devuan takes care of an abandoned Debian
package, and introduces a new package to manage mount points
automatically.  Go Devuan developers, go!

Before we get to the beta, __a new alpha image was released today__ to
fill the gap that has been widening between the current version of
Devuan packages and the existing alpha image.  This should be the last
alpha version before beta.  The gap between the next alpha and beta
will certainly be much shorter: not only we'd rather have a beta soon,
that can already be used in staging environments by professionals,
we're also aiming at releasing it before Debian Wheezy becomes
unsupported.

This issue is mostly the work of @dev1fanboy, who's been regularly
reading and annotating the mailing list topics.  As you may have
noticed, we, Devuan Editors, have not been able to sustain a weekly
issue of Devuan "Weekly" News.  Therefore we decided to move from a
"weekly" schedule to "when it's ready".  We hope you understand that
it's not a lack of commitment, but rather a lack of means.  With more
participants we could certainly produce a more regular newsletter:
this is your news, and you can make it!  Moreover, we don't really
know who's reading, and whether this newsletter fulfills an actual
need. We'd appreciate feedback about how we could better the
newsletter -- what works, what doesn't and what's missing.  Please use
the subject "About Devuan News" on the mailing list to comment on this
topic.  Thank you!

-- @hellekin

## Breaking News

### [NEW Devuan Alpha4 Released][Files]

Daniel Reurich released an updated alpha image: `alpha2` was released
7 months ago and a lot of packages were updated since, including many
security updates.  `alpha3` was built but not distributed--because the
main change was the splash image, and not much else.  One of the
recent Linux security updates broke `alpha2`: most of the filesystem
modules were broken with symbol mismatch.  You can
[get the new `alpha4`][Files] from `files.devuan.org`.

## Lately in Devuan

### [Progress has been made with desktop-base][1]

Daniel Reurich reports that he has been hacking on the desktop-base
package to add new features and make the package easier to maintain in
the future. He mentions that the Devuan version now has little
resemblance to the original desktop-base package from Debian, and is
asking people to fork the package on gitlab and make merge requests
for any proposed changes.  Progress on this means that the package is
almost complete, bringing us closer to the anticipated beta release.

To repeat and insist on @CenturionDan's call: "__Please fork and make
merge requests with any proposed changes.__"

### [How to use the ip command][2]

Steve Litt tours automatic network configuration in Debian, and points
out an easy way to configure the network that doesn't rely on the
system scripts. He shows how to use the `ip` command with a script he
provided that brings up network interfaces in a simple and distro
agnostic way.

### [Devuan adopts the kbd package][3]

Clarke Sideroad gives a heads-up: Debian no longer supports the kbd
package for console configuration in Debian Sid, which also effects
Devuan Ceres (unstable).  Daniel Reurich is
[preparing the package for unstable][4] so that it may be handed over
to a maintainer.  Once this happens the kbd package will be included
in unstable, offering users the choice to continue using it to
configure the console.

### [Devuan merged and debian multimedia repos will be separated][5]

Recently @golinux asked why the Debian "multimedia" repository is
being included in the Devuan "merged" repo. She's concerned about the
history of issues when using the Debian "multimedia" repo with Debian,
and Simon Wise confirms that
[this could cause some problems on Devuan][6].  Daniel Reurich
explains that [this was mostly done as a test for amprolla][7] and
later [raises an issue about the problem][8].  Once the repositories
are separated Devuan will remained unaffected by any potential related
problems.

## New projects for Devuan

### [Amounter][9]

As mentioned in the previous issue `amounter` is an auto-mounting
program written by Steve Litt in the Python programming language, with
the aims of having minimal dependencies and avoiding complications.
Amounter runs independently of file managers, window managers, and
desktop environments and is suitable for use with the console. The
author [announced its release in late December][10] on the DNG mailing
list. Since `amounter` is in a usable state it will soon enter the
experimental branch and will later be backported to the stable branch.

## Devuan's Not Gnome

DNG is the discussion list of the Devuan Project.

- [Subscribe to the list][subscribe]
- [Read online archives][archives]

-------------------
Read you soon!

Devuan News is made by your peers: you're [welcome to contribute][wiki]!

* @hellekin (editor at large)
* @golinux (den mother)
* @CenturionDan (Devuan's packaging master)
* @dev1fanboy (essayist)

[1]: https://lists.dyne.org/lurker/message/20160121.213644.94012181.en.html "Daniel Reurich is asking for reviews of the desktop-base package"
[2]: https://lists.dyne.org/lurker/message/20160118.185942.02c53752.en.html "Steve Litt's post about the ip command"
[3]: https://lists.dyne.org/lurker/message/20160108.202837.0aea8128.en.html "Clarke Sideroad points out the deprecation of kbd"
[4]: https://lists.dyne.org/lurker/message/20160109.204216.bc23ef79.en.html "Daniel Reurich pulled the kbd package into the utils group"
[5]: https://lists.dyne.org/lurker/message/20160103.175248.5e442ab9.en.html "golinux asked why the Devuan merged repo includes the debian multimedia repo"
[6]: https://lists.dyne.org/lurker/message/20160104.142201.50983b43.en.html "Simon Wise confirmed debian multimedia repos can cause problems"
[7]: https://lists.dyne.org/lurker/message/20160104.223340.3d1c6f9b.en.html "Daniel Reurich explains the debian multimedia repos are a test for amprolla"
[8]: https://lists.dyne.org/lurker/message/20160104.234518.18d2d0f4.en.html "Daniel Reurich raised an issue about debian multimedia being in merged"
[9]: https://github.com/stevelitt/amounter "Steve Litt's amounter"
[10]: https://lists.dyne.org/lurker/message/20151230.073439.78cf3a92.en.html "Steve Litt announced amounter"
[Files]: https://files.devuan.org/
[subscribe]: https://mailinglists.dyne.org/cgi-bin/mailman/listinfo/dng "Subscribe to DNG"
[archives]: https://lists.dyne.org/lurker/list/dng.en.html "Read DNG List Archive"
[upcoming]: https://git.devuan.org/devuan-editors/devuan-news/wikis/upcoming-issue "Upcoming Issue"
[wiki]: https://git.devuan.org/devuan-editors/devuan-news/wikis/home "Devuan News"
