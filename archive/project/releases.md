# Devuan Releases

You can [read an explanation of devuan release codenames](devuan-codenames).

## Stable

There is no release yet.

Upcoming `stable` release codename is [Jessie](releases/jessie-1.0).

## Testing

Current `testing` release codename is [Ascii](releases/ascii-2.0).

## Unstable

The `unstable` release codename is [Ceres](releases/ceres), also known as `sid`, after debian's `unstable` codename.

