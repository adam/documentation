# Migrar para o Devuan Jessie
Este documento descreve como migrar para o Devuan Jessie a partir do Debian.

## Executando a migração
Precisamos editar a lista de espelhos para que possamos definir os repositórios Devuan como a nova fonte para os pacotes.

`root@debian:~# editor /etc/apt/sources.list`

A seguinte mudança aponta seu gerenciador de pacotes aos repositórios Devuan, portanto desabilite os repositórios Debian inserindo uma cerquilha nas linhas obsoletas.

~~~
deb http://pkgmaster.devuan.org/merged jessie main
deb http://pkgmaster.devuan.org/merged jessie-updates main
deb http://pkgmaster.devuan.org/merged jessie-security main
deb http://pkgmaster.devuan.org/merged jessie-backports main
~~~

Antes que possamos buscar pacotes do novo repositório, precisamos atualizar os arquivos de índice de pacotes.

`root@debian:~# apt-get update`

O chaveiro do Devuan é necessário para autenticação dos pacotes.

`root@debian:~# apt-get install devuan-keyring --allow-unauthenticated`

Agora que o chaveiro está instalado você deve atualizar os índices novamente para que os pacotes estejam autenticados a partir de agora.

`root@debian:~# apt-get update`

Podemos agora migrar para o Devuan. Selecione slim como gerenciador de exibição padrão se for questionado.

`root@debian:~# apt-get dist-upgrade`

Para remover systemd como pid1, uma reincialização do sistema é necessária.

`root@devuan:~# reboot`

## Tarefas de pós migração
Se você usava o GNOME no Debian antes da migração, recomendo mudar o gerenciador de sessão para o startxfce4.

`root@devuan:~# update-alternatives --config x-session-manager`

Componentes systemd podem ser removidos do sistema agora.

`root@devuan:~# apt-get purge systemd systemd-shim`

Se você não estiver usando D-Bus, poderá remover libsystemd0.

`root@devuan:~# apt-get purge libsystemd0`

Elimine quaisquer pacotes órfãos deixados por sua antiga instalação do Debian.

`root@devuan:~# apt-get autoremove --purge`

Essa é uma boa hora de limpar pacotes arquivados pelo APT de sua antiga instalação.

`root@devuan:~# apt-get autoclean`

---
Traduzido por Douglas H. Silva

<sub>**Esta obra está licenciada com uma Licença Creative Commons Atribuição-CompartilhaIgual 4.0 Internacional [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR)]. Todas as marcas são propriedades de seus respectivos donos. Este trabalho é fornecido "COMO É" e vem com absolutamente NENHUMA garantia.**</sub>
