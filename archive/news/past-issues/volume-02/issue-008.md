# [Devuan Weekly News][1] Issue VIII

__Volume 02, Week 3, Devuan Week 8__ https://git.devuan.org/Envite/devuan-weekly-news/wikis/past-issues/volume-02/issue-008

## Editorial

Welcome to Devuan Weekly News issue VIII.  After two months has passed
from the start of the project (or better, from the start of the [DNG
mailing list][archives]), it seems that trolls have mostly disappeared
and the list concentrates on discussing technical issues and solutions
in a collaborative environment.

'Collaborative' is a very beautiful meaning, and we have used it for
[DWN][1] as well. This **Issue VIII** comes from the collaboration of
**hellekin**, to which [DWN][1] is in debt for setting up the
processes to make this happen. He has also helped me sleep better with
his reading and resuming effort.

Besides, it seems that the week can be resumed with
[a sentence from karl][tagline]:
> if desktop users wants their usb-disks to be automatically mounted,
> let them, but don't force me.

It seems to be the true spirit of Devuan: freedom to choose.

## Last Week on Devuan

### [Jessie Without Systemd][2]

The exploration of TRIOS continues. Dragan warns the LiveCD does not
allow removing the CD upon shutdown, leading to unexpected reboot on
the TRIOS system. Renaud OLGIATI advises to tell the BIOS to boot on
HDD and override the boot sequence to boot on the CD.

karl announces his intention to publish [udev-independent packages][3]
to address the fact that some "packages depend on udev for wrong
reasons."

### [Use/Misuse of Depends][4]

cyteen wonders about the dependency policy in Devuan for using Depends
or Recommends when upstream is adding support for, but not reliance
on, some other package.  Recently the Debian policy changed to pull in
Recommends by default.  The consensus in this thread is that
Recommends should not be installed by default, and metapackages should
be used instead to satisfy all use cases. Noel remembers the exact
definitions of the current Debian policy.

That way both novices and experts would be satisfied: novices will use
metapackages and Tasks that pull in Recommends, and experts will be
able to choose for themselves.  Joel Roth suggests the
`--no-install-{recommends,suggests}` could have their counterparts,
e.g., `apt-get install pkgname --install-recommends`, to ramp up
dependencies.

### [Minimal Init][5]

Karl Hammar reports his experiments on choosing init on-the-fly,
inspired by [Manjaro experiments][6].  Isaac Dunham notes that such a
script would require running as PID 1 (and then exec(1P) to the
relevant executable).  Karl concludes it might be easier to use the
kernel's `init=` command line argument.

### [Itches and Scratches][7]

Gordon Haverland: "The reason Devuan exists, is an itch named
systemd. Should we be looking for other itches?" He wonders what if
someone scratch makes others itch, and how
itches and scratches pass from a distro to the next.  He discovered
that using `debootstrap` on Gentoo does not work as expected, and
points to dpkg.

Adam Borowski answers that dpkg is rock stable and its files are
almost uncorruptible due to all the care dpkg exercises on them.
Gordon then clarified dpkg did not corrupt the files, but that he
was trying to use debootstrap on Gentoo.

### [What to do with udev? Some ideas...][9]

Karl Hammar reactivates this thread with some experiments he made
[building X without udev support][10].

### [Purpose of all this complicated device management?][11]

Karl Hammar comes back on the alleged superiority of a dynamic device
manager and argues for freedom of choice: "if desktop users wants
their usb-disks to be automatically mounted, let them, but don't force
me."

Gravis and Jude Nelson argue in favor of vdev: it won't touch static
devices, it may even create detected devices and exit.  Isaac Dunham
notes that `busybox mdev` does the latter. Karl and Isaac discuss
scanning and mapping devices to kernel modules.

The thread [evolved][13] into a discussion about libsysdev, a library
to substitute udev's libsysfs which is being created by Isaac Dunham.

### [Wiki Spam][8]

Go Linux warns the VUA that spam should be *prevented* instead of
erased on the without-systemd.org wiki.

### [UEFI and GPT][12]

Robert Storey wonders if Devuan will support UEFI boot and GPT
partition tables. It seems Debian does not boot properly with those
parameters, and Devuan should. Gravis notes that Devuan's first
release will be almost the same as Debian Jessie but without systemd,
and remembers hardware may be necessary for testing afterwards.

## Devuan's Not Gnome

DNG is the discussion list of the Devuan Project.

- [Subscribe to the list][subscribe]
- [Read online archives][archives]

-------------------
No sleepless Monday night this week, thanks to the collaboration of **hellekin**.

You can collaborate too!
Devuan Weekly News is made by your peers: you're [welcome to contribute][wiki]!

Thanks for reading so far.
Read you next week!

DWN Team
* Noel, "er Envite" (Editor)
* hellekin (Co-writer, markup master)


[1]: https://git.devuan.org/Envite/devuan-weekly-news/wikis/current-issue "Current Issue"
[2]: https://lists.dyne.org/lurker/message/20150112.192838.18133400.en.html "Jessie Without Systemd"
[3]: http://turkos.aspodata.se/computing/nonudev/ "non-udev packages"
[4]: https://lists.dyne.org/lurker/message/20150113.105018.6adf5569.en.html "Use and misuse of Depends"
[5]: https://lists.dyne.org/lurker/message/20150117.221346.2116e3b6.en.html "Minimal Init"
[6]: http://www.troubleshooters.com/linux/init/manjaro_experiments.htm "Manjaro Experiments"
[7]: https://lists.dyne.org/lurker/message/20150116.215432.88ad9ffa.en.html "Itches and Scratches"
[8]: https://lists.dyne.org/lurker/message/20150117.222138.02c656d9.en.html "Wiki Spam"
[9]: https://lists.dyne.org/lurker/message/20150117.183508.523afdf1.en.html "What to do with udev? Some ideas..."
[10]: http://turkos.aspodata.se/computing/sketchy_notes_on_X_install "udev-less X"
[11]: https://lists.dyne.org/lurker/message/20150117.174913.1017259a.en.html "Purpose of all this complicated device management?"
[12]: https://lists.dyne.org/lurker/message/20150119.143642.4c8a9419.en.html "UEFI and GPT"
[13]: https://lists.dyne.org/lurker/message/20150119.190115.cb63c930.en.html "libsysdev preview"
[tagline]: https://lists.dyne.org/lurker/message/20150117.174913.1017259a.en.html "Don't force me"
[subscribe]: https://mailinglists.dyne.org/cgi-bin/mailman/listinfo/dng "Subscribe to DNG"
[archives]: https://lists.dyne.org/lurker/list/dng.en.html "Read DNG List Archive"
[wiki]: https://git.devuan.org/Envite/devuan-weekly-news/wikis/home "Devuan Weekly News"
