# [Devuan News](..) Archives

Archives are stored by yearly volume, each sorted by month (DN Archives uses [Holocene era](../why-he) for dating years).

The [first year of Devuan Weekly News](volume-01) which is now known as Devuan News (DN) counted 4 issues,
all were the work of Noel "Envite" Torres.

The [second year of Devuan News](volume-02) changed its name to Dvuan News starting with issue XXIV, currently counts 11 issues,
worked out by hellekin and edited by Noel "Envite" Torres and etech3, with help from golinux. Mincer.

- [Volume 02](volume-02) (12015, 11 issues, current)
- [Volume 01](volume-01) (12014, 4 issues)
