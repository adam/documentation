Questa guida è valida unicamente per le migrazioni dalla versione Jessie di Debian. Nelle migrazioni verso Beowulf le istruzioni sono specifiche per la versione Debian dalla quale si proviene e vanno seguite scrupolosamente.

# Migrazione da Debian Jessie a Beowulf

Per cominciare è necessario [configurare la rete](network-configuration.md), altrimenti perderete l'accesso durante la migrazione.

Il modo migliore e più semplice per dare avvio alla migrazione è installando sysvinit-core.

`root@debian:~# apt-get install sysvinit-core`

Poi riavviate in modo che sysvinit sia pid 1.

`root@debian:~# reboot`

Gli utenti Gnome dovranno rimuovere lo "gnome tweak tool" per evitare situazioni di blocco dei pacchetti. Ovviamente, l'ambiente desktop potrebbe risentirne, ma se lo si desidera, sarà possibile installare nuovamente i pacchetti Gnome di Devuan a migrazione avvenuta.

`root@debian:~# apt-get purge gnome-tweak-tool`

Modificate il file sources.list di modo che punti ai repository Beowulf.

`root@debian:~# editor /etc/apt/sources.list`

Modificate sources.list come descritto qui di seguito. Commentate tutto il resto.

~~~
deb http://deb.devuan.org/merged beowulf main
deb http://deb.devuan.org/merged beowulf-updates main
deb http://deb.devuan.org/merged beowulf-security main
#deb http://deb.devuan.org/merged beowulf-backports main
~~~

Aggiornate la lista dei pacchetti dai repository Beowulf.

`root@debian:~# apt-get update`

Installate il "portachiavi" Devuan così da autenticare i pacchetti seguenti.

`root@debian:~# apt-get install devuan-keyring --allow-unauthenticated`

Aggiornate nuovamente l'indice dei pacchetti così che siano autenticati con il "portachiavi".

`root@debian:~# apt-get update`

Se nel corso della configurazione della rete si è scelto di usare wicd, bisognerà prima effettuare un aggiornamento semplice dai repository Beowulf.

`root@debian~# apt-get upgrade`

Per poi migrare.

`root@debian:~# apt-get dist-upgrade`

Se l'aggiornamento si interrompe dovrete risolvere il problema relativo ai pacchetti discordanti e ripartire.

`root@debian:~# apt-get -f install`  
`root@debian:~# apt-get dist-upgrade`

Rimuovete systemd.

`root@debian:~# apt-get purge systemd`

Coloro che intendono usare Gnome dovrebbero installare il relativo "task".

`root@devuan:~# apt-get install task-gnome-desktop`

In alternativa si può usare XFCE, l'ambiente desktop di default di Devuan.

`root@devuan~# apt-get install task-xfce-desktop`

Molto probabilmente la migrazione lascerà uno strascico di pacchetti "orfani" e archivi inservibili. Rimuoveteli per sgrassare il sistema.

`root@devuan:~# apt-get autoremove --purge`  
`root@devuan:~# apt-get autoclean`

---

<sub>Traduzione di antoniotrkdz<sub>

<sub>**Questa guida è rilasciata in base alla licenza Creative Commons Attribuzione - Condividi allo stesso modo 4.0 Internazionale [[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.it)]. Tutti i marchi sono proprietà dei rispettivi depositari. Le informazioni in questa guida vengono fornite "COME SONO" e non sono coperte da alcuna garanzia.**</sub>
