# [Devuan Weekly News][current] Issue X

__Volume 02, Week 5, Devuan Week 10__

https://git.devuan.org/Envite/devuan-weekly-news/past-issues/volume-02/issue-010

## Editorial

The public surface of the project grows with the addition of new
communication channels, mentioned below. This seems to mean that we
are building a sane community. Devuan is still a small group of people
with different interests and with even more different goals, but we
work by polite discussion and we are coming to common grounds to
make a shared effort to produce a free Linux distribution.

And free, for us, means that the user should have the freedom to
choose which software pieces run on her computer.


## Announcement

Devuan now has an official Twitter account @DevuanOrg and an official
Google+ community Devuan Linux. DWN will include news from those sources:

+ Twitter account: https://twitter.com/DevuanOrg/status/560915541049610240
+ Google+ Community: https://plus.google.com/u/0/communities/107769028740033535625


## Last Week in Devuan

### [Versioning][1]

The thread about how to number the Devuan versions of Debian packages
continues on with discussions about the bumping problem (when Debian
bumps its own version number) and the pinning solution. Noel "er
Envite" explained how pinning works, and there were rough consensus,
started by Adam Borowski, about pinning our repository above Debian
repositories but not above priority 1000, and not even above 990.

### [Boot loader][2]

Steve Litt raised the issue of the boot loader for Devuan, stating
that GRUB 2 is as complex as systemd. He was pointed to GRUB Legacy,
LILO and ELILO, and syslinux and extlinux were named as well. About
GRUB 2 itself, Gravis [pointed out][3] that

> there aren't any dependencies on any bootloader that aren't for
> configuration tools, so unlike systemd you can replace it at will

Discussion continued with iPXE by q9c9p saying:

> I would advocate what has already being state in dng, given the
> choice let the users decide what they want.

with which we at DWN fully agree. Discussion then turned to selecting
defaults, and to the necessity for providing documentation.

### [systemd development][4]

Martijn Dekkers reports a talk about upcoming systemd features. This
launched a very long and dense thread, with several branches. As it
can be easily imaginated, most of the comments were on the complaining
or joking side (or both), but there are interesting points.

One of them is t.j.duchene saying that [systemd is here to stay][5]
and that we need a long term solution, plus other comments. This was
refuted.

Anthony G. Basile pointed out that he maintains eudev and [requested
help][6].

Jude Nelson remembered that systemd's ability to hide devices [belongs
to vdev as well][7]. He also talked about collaboration with Anthony
G. Basile to have a [single libudev compatibility library][8].

Steve Litt provided a comprehensive list of reasons for which it is
[not advisable to try to change systemd from within][9].

tilt made the very [important point to remember][10] that:

> we are no "sworn enemies of SystemD", of the
> project, its members or contributors or the motivations,
> ideas or even intentions it tries to implement.

### [Mathematical perspective][11]

wiliam moss provided a mathematical view of why systemd can not be
patched to make it better.


## New Releases

### [libsysdev v0.1.0][12]

Jack L. Frost announced that the first version of libsysdev and
xf86-input-evdev are now packaged for Arch Linux.

Source code: https://github.com/idunham/libsysdev

Arch Linux packages:
- https://git.fleshless.org/pkgbuilds/tree/libsysdev 
- https://git.fleshless.org/pkgbuilds/tree/xf86-input-evdev-libsysdev 


## Devuan's Not Gnome

DNG is the discussion list of the Devuan Project.

- [Subscribe to the list][subscribe]
- [Read online archives][archives]

-------------------
We really want you to [help us make DWN][wiki]. Feel free to edit. DWN
is made by your peers.

You can also join and criticize us at IRC: #devuan-news at freenode

Thanks for reading so far.
Read you next week!

DWN Team
* Noel, "er Envite" (Editor)
* hellekin (Co-writer, markup master)


[1]: https://lists.dyne.org/lurker/message/20150129.181010.b640617f.en.html "Versioning"
[2]: https://lists.dyne.org/lurker/message/20150130.223928.216bd6f0.en.html "Boot loader"
[3]: https://lists.dyne.org/lurker/message/20150131.011810.35575b5f.en.html "GRUB 2 forces no dependencies"
[4]: https://lists.dyne.org/lurker/message/20150202.110251.183a1992.en.html "systemd development"
[5]: https://lists.dyne.org/lurker/message/20150202.225412.155056fb.en.html "systemd is here to stay"
[6]: https://lists.dyne.org/lurker/message/20150202.210653.1320fb4b.en.html "help needed on eudev"
[7]: https://lists.dyne.org/lurker/message/20150202.111241.8e8c211b.en.html "vdev can hide devices as well"
[8]: https://lists.dyne.org/lurker/message/20150202.212627.5308ff10.en.html "libudev compatibility layer decoupled from vdev and eudev"
[9]: https://lists.dyne.org/lurker/message/20150202.184604.493c7d9f.en.html "not to change systemd from within"
[10]: https://lists.dyne.org/lurker/message/20150203.003913.edd7fd4e.en.html "we are no sworn enemies of systemd"
[11]: https://lists.dyne.org/lurker/message/20150202.223120.390bc812.en.html "Mathematical perspective"
[12]: https://lists.dyne.org/lurker/message/20150130.174613.c81fbdd0.en.html "libsysdev v0.1.0"

[subscribe]: https://mailinglists.dyne.org/cgi-bin/mailman/listinfo/dng "Subscribe to DNG"
[archives]: https://lists.dyne.org/lurker/list/dng.en.html "Read DNG List Archive"
[current]: https://git.devuan.org/Envite/devuan-weekly-news/wikis/current-issue "Current Issue"
[wiki]: https://git.devuan.org/Envite/devuan-weekly-news/wikis/home "Devuan Weekly News"
