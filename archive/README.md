# Archived docs

## devuan project docs

* These documents contain information of historical interest that set the course for Devuan in the weeks and months following Devuan's announcement of the intention to fork on [2014-11-27](https://www.devuan.org/os/announce/). They were mainly written by Franco Lanza, Jaromil and hellekin.

## devuan news docs

*  Shortly after the fork, Noel Torres initiated the "Devuan Weekly News", an email summary of important discussions and events in the fork's progress. There were 4 issues in December 2014. Other editors and contributors joined the Devuan News team in 2015 however, the final issue for that year was in June. There were 2 issues in 2016, that were the last published, making a total of 15 issues.

## dev1fanboy's wiki and pages

* Chillfan’s aka dev1fanboy's wiki made its first appearance with the launch of a redesigned [devuan.org](http://web.archive.org/web/20160604151752/http://www.devuan.org/) prior to the jessie release. At the time it was not integrated into the website structure but instead accessed directly from git.devuan.org.


